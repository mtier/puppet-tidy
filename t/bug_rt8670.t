use strict;
use Puppet::Tidy;
use Test::More tests => 1;

my (@should_be_output, @output, $source);

###
# Do not insist on quoting the 'default' keyword.
###
$source = << 'EOF';
case $operatingsystem {
  default: { include role::generic }
  default: { include stdlib }
}

EOF

@should_be_output = << 'EOF';
case $operatingsystem {
  default: { include role::generic }
  default: { include stdlib }
}

EOF

Puppet::Tidy::puppettidy(source => $source, destination => \@output);
is_deeply(@output, @should_be_output, "Don't quote the default keyword");
