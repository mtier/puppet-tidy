use strict;
use Puppet::Tidy;
use Test::More tests => 1;

my (@should_be_output, @output, $source);

###
# Don't unquote MD5 hashed passwords.
###
$source = << 'EOF';
user { 'abcuser':
     ensure => present,
     comment => 'User',
     home => '/home/abcuser',
     password => '$1$h3e22.7M$Nb22222222345678990443',
}
EOF

@should_be_output = << 'EOF';
user { 'abcuser':
     ensure => present,
     comment => 'User',
     home => '/home/abcuser',
     password => '$1$h3e22.7M$Nb22222222345678990443',
}
EOF

Puppet::Tidy::puppettidy(source => $source, destination => \@output);
is_deeply(@output, @should_be_output, "Don't unquote password lines");
